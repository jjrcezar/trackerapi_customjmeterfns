package com.trackerv2.jmeter.functions;

import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.functions.AbstractFunction;
import org.apache.jmeter.functions.InvalidVariableException;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.apache.jmeter.threads.JMeterVariables;

import java.util.*;

public class EpochGenerator extends AbstractFunction {
    private static final List<String> DESC = Arrays.asList("Custom function to get epoch time");
    private static final String KEY = "__getEpoch";
    private List<CompoundVariable> parameters = Collections.emptyList();

    @Override
    public String execute(SampleResult arg0, Sampler arg1) throws InvalidVariableException {
        String epoch = String.valueOf(System.currentTimeMillis());
        this.storeResultsToVariable(epoch);
        return epoch;
    }

    private void storeResultsToVariable(String value) {
        if (null == parameters || parameters.size() == 0) return;
        String resultsVariable = parameters.get(parameters.size() - 1).execute().trim();

        JMeterVariables vars = getVariables();
        if (null != vars && value.length() > 0) {
            vars.put(resultsVariable, value);
        }
    }

    @Override
    public String getReferenceKey() {
        return KEY;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setParameters(Collection arg0) throws InvalidVariableException {
        parameters = new ArrayList<CompoundVariable>(arg0);
    }

    @Override
    public List<String> getArgumentDesc() {
        return DESC;
    }

}
