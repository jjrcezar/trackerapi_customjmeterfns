package com.trackerv2.jmeter.functions;

import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.functions.AbstractFunction;
import org.apache.jmeter.functions.InvalidVariableException;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.apache.jmeter.threads.JMeterVariables;

import java.util.*;

public class FilterGenerator extends AbstractFunction {
    private static final List<String> DESC = Arrays.asList("Custom function to get random filters");
    private static final String KEY = "__getRandomFilters";
    private List<CompoundVariable> parameters = Collections.emptyList();
    private List<String> filters = null;

    private void initFilters() {
        if (null == this.filters) {
            this.filters = new ArrayList<String>();
        }
        if (this.filters.size() == 0) {
            this.filters.add("all");
            this.filters.add("min");
            this.filters.add("minimized");
            this.filters.add("domains");
            this.filters.add("affils");
            this.filters.add("optouts");
        }
    }

    private int getRandomCount() {
        return new Random().nextInt((this.filters.size() - 1) + 1) + 1;
    }

    public String getRandomFilters() {
        this.initFilters();
        Collections.shuffle(this.filters);
        return String.join(",", this.filters.subList(0, getRandomCount()));
    }

    @Override
    public String execute(SampleResult arg0, Sampler arg1) throws InvalidVariableException {
        return this.getRandomFilters();
    }

    private void storeResultsToVariable(String value) {
        if (null == parameters || parameters.size() == 0) return;
        String resultsVariable = parameters.get(parameters.size() - 1).execute();

        JMeterVariables vars = getVariables();
        if (null != vars && value.length() > 0) {
            vars.put(resultsVariable, value);
        }
    }

    @Override
    public String getReferenceKey() {
        return KEY;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setParameters(Collection arg0) throws InvalidVariableException {
        parameters = new ArrayList<CompoundVariable>(arg0);
    }

    @Override
    public List<String> getArgumentDesc() {
        return DESC;
    }

}
