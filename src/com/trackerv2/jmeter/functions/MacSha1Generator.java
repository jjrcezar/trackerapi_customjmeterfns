package com.trackerv2.jmeter.functions;

import org.apache.commons.codec.binary.Base64;
import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.functions.AbstractFunction;
import org.apache.jmeter.functions.InvalidVariableException;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.apache.jmeter.threads.JMeterVariables;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;


public class MacSha1Generator extends AbstractFunction {
    private static final List<String> DESC = Arrays.asList("Custom function to generate MAC-SHA1");
    private static final String KEY = "__getMacSha1";
    private List<CompoundVariable> parameters = Collections.emptyList();

    public String getMacSha1(String date, String publicKey, String privateKey) {
        SecretKey secretKey;
        Mac mac;
        String toHash = date + publicKey;
        String macSha1 = null;

        try {
            secretKey = new SecretKeySpec(Base64.decodeBase64(privateKey.getBytes(Charset.forName("ISO-8859-1"))),
                    "HmacSHA1");
            mac = Mac.getInstance("HmacSHA1");
            mac.init(secretKey);
            macSha1 = new String(Base64.encodeBase64(mac.doFinal(toHash.getBytes(Charset.forName("ISO-8859-1")))));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        return macSha1;
    }

    @Override
    public String execute(SampleResult arg0, Sampler arg1) throws InvalidVariableException {
        List<String> resolvedArgs = new ArrayList<String>(parameters.size());
        for (CompoundVariable parameter : parameters) {
            resolvedArgs.add(parameter.execute().trim());
        }
        return this.getMacSha1(resolvedArgs.get(0), resolvedArgs.get(1), resolvedArgs.get(2));
    }

    private void storeResultsToVariable(String value) {
        if (null == parameters || parameters.size() == 0) return;
        String resultsVariable = parameters.get(parameters.size() - 1).execute();

        JMeterVariables vars = getVariables();
        if (null != vars && value.length() > 0) {
            vars.put(resultsVariable, value);
        }
    }

    @Override
    public String getReferenceKey() {
        return KEY;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setParameters(Collection arg0) throws InvalidVariableException {
        parameters = new ArrayList<CompoundVariable>(arg0);
    }

    @Override
    public List<String> getArgumentDesc() {
        return DESC;
    }
}
