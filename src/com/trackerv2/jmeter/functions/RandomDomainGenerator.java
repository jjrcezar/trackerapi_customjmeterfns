package com.trackerv2.jmeter.functions;

import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.functions.AbstractFunction;
import org.apache.jmeter.functions.InvalidVariableException;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.apache.jmeter.threads.JMeterVariables;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class RandomDomainGenerator extends AbstractFunction {
    private static final List<String> DESC = Arrays.asList("Random Domain Generator");
    private static final String KEY = "__getRandomDomain";
    private List<CompoundVariable> parameters = Collections.emptyList();
    private List<String> domainList = new ArrayList<>();

    private String getRandomDomain() {
        this.initializeDomainList();
        int max = this.domainList.size();
        int randomIndex = new Random().nextInt(max);
        return this.domainList.get(randomIndex); 
    }

    private void initializeDomainList() {
        if (this.domainList.size() != 0) {
            return;
        }

        File file;
        BufferedReader br;
        String domain;
        try {
            file = new File("domain_list.csv");
            br = new BufferedReader(new FileReader(file));

            while ((domain = br.readLine()) != null) {
                domainList.add(domain);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String execute(SampleResult arg0, Sampler arg1) throws InvalidVariableException {
        return this.getRandomDomain();
    }

    private void storeResultsToVariable(String value) {
        if (null == parameters || parameters.size() == 0) return;
        String resultsVariable = parameters.get(parameters.size() - 1).execute();

        JMeterVariables vars = getVariables();
        if (null != vars && value.length() > 0) {
            vars.put(resultsVariable, value);
        }
    }

    @Override
    public String getReferenceKey() {
        return KEY;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setParameters(Collection arg0) throws InvalidVariableException {
        parameters = new ArrayList<CompoundVariable>(arg0);
    }

    @Override
    public List<String> getArgumentDesc() {
        return DESC;
    }

}
