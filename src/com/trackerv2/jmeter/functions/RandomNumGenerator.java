package com.trackerv2.jmeter.functions;

import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.functions.AbstractFunction;
import org.apache.jmeter.functions.InvalidVariableException;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.apache.jmeter.threads.JMeterVariables;

import java.util.*;

public class RandomNumGenerator extends AbstractFunction {
    private static final List<String> DESC = Arrays.asList("Random Number Generator");
    private static final String KEY = "__getRandomNum";
    private List<CompoundVariable> parameters = Collections.emptyList();

    private String getRandomNum(String minStr, String maxStr) {
        int min = 0;
        int max = 0;

        if (null != minStr && minStr.length() > 0) {
            min = Integer.parseInt(minStr);
        }
        if (null != maxStr && maxStr.length() > 0) {
            max = Integer.parseInt(maxStr);
        }

        return String.valueOf(new Random().nextInt((max - min) + 1) + min);
    }

    @Override
    public String execute(SampleResult arg0, Sampler arg1) throws InvalidVariableException {
        List<String> resolvedArgs = new ArrayList<String>(parameters.size());
        for (CompoundVariable parameter : parameters) {
            resolvedArgs.add(parameter.execute().trim());
        }
        return this.getRandomNum(resolvedArgs.get(0), resolvedArgs.get(1));
    }

    private void storeResultsToVariable(String value) {
        if (null == parameters || parameters.size() == 0) return;
        String resultsVariable = parameters.get(parameters.size() - 1).execute();

        JMeterVariables vars = getVariables();
        if (null != vars && value.length() > 0) {
            vars.put(resultsVariable, value);
        }
    }

    @Override
    public String getReferenceKey() {
        return KEY;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setParameters(Collection arg0) throws InvalidVariableException {
        parameters = new ArrayList<CompoundVariable>(arg0);
    }

    @Override
    public List<String> getArgumentDesc() {
        return DESC;
    }

}